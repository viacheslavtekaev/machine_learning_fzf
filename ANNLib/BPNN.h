﻿#pragma once
#include <ANN.h>
using namespace ANN;

class BPNN :
	public ANeuralNetwork
{
public:
	ANNDLL_API std::vector<float> Predict(std::vector<float> & input);
	ANNDLL_API std::string GetType();
	bool ANNDLL_API Train(std::vector<std::vector<float>> & inputs,
						  std::vector<std::vector<float>> & outputs,
						  int maxIters = 1000000,
						  float eps = 0.001,
						  float speed = 0.1,
						  bool std_dump = false);
	void ANNDLL_API setConfiguration(std::vector<int>& config, ActivationType activation_type = ANeuralNetwork::POSITIVE_SYGMOID);

	ANNDLL_API void InitWeights();

	ANNDLL_API void ChangeWeight(int layer, int inum, int onum, float weight);

	/**
		* Прочитать нейронную сеть из файла. Сеть сохраняется вызовом метода Save
		* @param filepath - имя и путь до файла с сеткой
		* @return - успешность считывания
		*/
	ANNDLL_API bool Load(std::string filepath) override;

	/**
	* Сохранить нейронную сеть в файл. Сеть загружается вызовом метода Load
	* @param filepath - имя и путь до файла с сеткой
	* @return - успешность сохранения
	*/
	ANNDLL_API bool Save(std::string filepath) override;

	ANNDLL_API std::vector<std::vector<std::vector<float>>> GetWeights();

	ANNDLL_API BPNN();
	ANNDLL_API BPNN(BPNN *src);
	ANNDLL_API BPNN(std::shared_ptr<BPNN> src);
	ANNDLL_API ~BPNN();
};


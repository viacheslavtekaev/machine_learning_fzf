#define ANNDLL_EXPORTS
#include "BPNN.h"
#include <iostream>
#include <fstream>
#include <iomanip>



std::vector<float> BPNN::Predict(std::vector<float>& input)
{
	if (input.size() != configuration[0])
	{
		std::cout << "wrong amount of input data";
		return std::vector<float>();
	}

	std::vector<std::vector<float>> tmp(configuration.size());

	tmp[0] = input;

	for (int curLayer = 0; curLayer < weights.size(); curLayer++)
	{
		//std::cout << "Wc [curLayer: " << curLayer << "] " << ann->weights[curLayer].size() << std::endl;
		tmp[curLayer + 1] = std::vector<float>(weights[curLayer].size(), 0);
		for (int innum = 0; innum < weights[curLayer].size(); innum++)
		{
			//std::cout << "Wc [curLayer innum: " << curLayer << ", " << innum << "] " << ann->weights[curLayer][innum].size() << std::endl;
			for (int onnum = 0; onnum < weights[curLayer][innum].size() - 1; onnum++)
			{
				//std::cout << "w[ " << curLayer << ", " << innum << ", " << onnum << "]: " << ann->weights[curLayer][innum][onnum] << "\t";
				tmp[curLayer + 1][innum] +=
					(weights[curLayer][innum][onnum] * (curLayer == 0 ?
						tmp[curLayer][onnum] :
						Activation(tmp[curLayer][onnum])));

				//printf("w[%d, %d, %d]: %.10f \t", curLayer, innum, onnum, ann->weights[curLayer][innum][onnum]);
				//std::cout << "tmp[ " << curLayer << ", 0, " << onnum << "]: " << fwdtmp[curLayer][onnum] << std::endl;
			}
			//printf("w[%d, %d, %d]: %.10f \t", curLayer, innum, ann->weights[curLayer][innum].size() - 1, ann->weights[curLayer][innum].bSeack());
			tmp[curLayer + 1][innum] += weights[curLayer][innum].back();
			//std::cout << "tmp[ " << curLayer + 1 << ", " << innum << ", 0]: " << fwdtmp[curLayer + 1][innum] << std::endl;
			//std::cout << std::endl;
			//printf("\n");
		}
		//printf("----------------\n");
		//std::cout << "-----";
		//std::cout << std::endl;
	}

	for (int i = 0; i < tmp.back().size(); i++)
	{
		tmp.back()[i] = Activation(tmp.back()[i]);
	}

	return tmp.back();
}

std::string BPNN::GetType()
{
	return std::string("NEURAL NETWORK BY VIACHESLAV TEKAEV\n");
}

float ANN::BackPropTrain(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	std::vector<std::vector<float>> &inputs,
	std::vector<std::vector<float>> &outputs,
	int maxIters,
	float eps,
	float speed,
	bool std_dump)
{
	
	return 1;
}

bool BPNN::Train(std::vector<std::vector<float>> & inputs,
				 std::vector<std::vector<float>> & outputs,
				 int maxIters,
				 float eps,
				 float speed,
				 bool std_dump)
{
	//return ANN::BackPropTrain(std::shared_ptr<ANN::ANeuralNetwork>(this), inputs, outputs, maxIters, eps, speed, std_dump);
	BPNN *ann = this;

	ann->InitWeights();

	//ann->is_trained = true;
	//return true;

	std::vector<std::vector<float>> fwdtmp(ann->configuration.size());
	std::vector<std::vector<float>> bwdtmp(ann->configuration.size());
	std::vector<float> errors(outputs[0].size());
	auto wd = weights;
	
	scale = 1;

	float delta = eps + 1;
	for (int learnStage = 0; learnStage < maxIters && delta > eps; learnStage++)
	{
		delta = 0;
		for (int inputNum = 0; inputNum < inputs.size(); inputNum++)
		{
			if (inputs[inputNum].size() != ann->configuration[0])
			{
				std::cout << "wrong amount of input data";
				return 0;
			}

			fwdtmp[0] = inputs[inputNum];

			//forward
			for (int curLayer = 0; curLayer < ann->weights.size(); curLayer++)
			{
				//std::cout << "Wc [curLayer: " << curLayer << "] " << ann->weights[curLayer].size() << std::endl;
				fwdtmp[curLayer + 1] = std::vector<float>(ann->weights[curLayer].size(), 0);
				for (int innum = 0; innum < ann->weights[curLayer].size(); innum++)
				{
					//std::cout << "Wc [curLayer innum: " << curLayer << ", " << innum << "] " << ann->weights[curLayer][innum].size() << std::endl;
					for (int onnum = 0; onnum < ann->weights[curLayer][innum].size() - 1; onnum++)
					{
						//std::cout << "w[ " << curLayer << ", " << innum << ", " << onnum << "]: " << ann->weights[curLayer][innum][onnum] << "\t";
						fwdtmp[curLayer + 1][innum] +=
							(ann->weights[curLayer][innum][onnum] * (curLayer == 0 ?
								fwdtmp[curLayer][onnum] :
								ann->Activation(fwdtmp[curLayer][onnum])));

						//printf("w[%d, %d, %d]: %.10f \t", curLayer, innum, onnum, ann->weights[curLayer][innum][onnum]);
						//std::cout << "tmp[ " << curLayer << ", 0, " << onnum << "]: " << fwdtmp[curLayer][onnum] << std::endl;
					}
					//printf("w[%d, %d, %d]: %.10f \t", curLayer, innum, ann->weights[curLayer][innum].size() - 1, ann->weights[curLayer][innum].back());
					fwdtmp[curLayer + 1][innum] += weights[curLayer][innum].back();
					//std::cout << "tmp[ " << curLayer + 1 << ", " << innum << ", 0]: " << fwdtmp[curLayer + 1][innum] << std::endl;
					//std::cout << std::endl;
					//printf("\n");
				}
				//printf("----------------\n");
				//std::cout << "-----";
				//std::cout << std::endl;
			}
			//printf("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFf\n");

			//calc deviation
			for (int i = 0; i < errors.size(); i++)
			{
				errors[i] = (outputs[inputNum][i] - ann->Activation(fwdtmp.back()[i])) * ann->ActivationDerivative(ann->Activation(fwdtmp.back()[i]));
				delta += pow((outputs[inputNum][i] - ann->Activation(fwdtmp.back()[i])), 2) / 2.;
				/*printf("good: %.f\npred: %.8f\ndelta: %.8f\nerror: %.8f\n", 
					outputs[inputNum][i], 
					ann->Activation(fwdtmp.back()[i]), 
					outputs[inputNum][i] - ann->Activation(fwdtmp.back()[i]),
					errors[i]);*/
				//std::cout << "der: " << ann->ActivationDerivative(fwdtmp.back()[i]) << std::endl;
				//std::cout << "sigma: " << errors[i] << std::endl;
			}
			

			bwdtmp.back() = errors;

			//backward
			for (int curLayer = ann->weights.size() - 1; curLayer >= 0; curLayer--)
			{
				bwdtmp[curLayer] = std::vector<float>(ann->weights[curLayer][0].size() - 1, 0);
				for (int innum = 0; innum < ann->weights[curLayer].size(); innum++)
				{
					for (int onnum = 0; onnum < ann->weights[curLayer][innum].size() - 1; onnum++)
					{
						bwdtmp[curLayer][onnum] += (ann->weights[curLayer][innum][onnum] * bwdtmp[curLayer + 1][innum]);

						//std::cout << "bwdtmp[ " << curLayer << ", " << onnum << "]: " << bwdtmp[curLayer][onnum] << " += ";
						//std::cout << "w[ " << curLayer << ", " << onnum << ", " << innum << "]: " << ann->weights[curLayer][innum][onnum] << " * ";			
						//std::cout << "bwdtmp[ " << curLayer + 1 << ", " << innum << "]: " << bwdtmp[curLayer + 1][innum] << std::endl << std::endl;

						if (innum == ann->weights[curLayer].size() - 1)
						{
							bwdtmp[curLayer][onnum] *= ann->ActivationDerivative(ann->Activation(fwdtmp[curLayer][onnum]));
							//std::cout << "bwdtmp[ " << curLayer << ", 0, " << onnum << "]: " << bwdtmp[curLayer][onnum] << std::endl;
							//std::cout << "der activation" << std::endl;
						}

						wd[curLayer][innum][onnum] = 
							bwdtmp[curLayer + 1][innum]
							* speed 
							* (curLayer == 0 ?
								fwdtmp[curLayer][onnum] :
								ann->Activation(fwdtmp[curLayer][onnum]));

					}
					wd[curLayer][innum].back() = bwdtmp[curLayer + 1][innum] * speed;
				}
				//std::cout << "*********";
				//std::cout << std::endl;
			}

			/*for (int i = 0; i < bwdtmp.size(); i++)
			{
				for (int j = 0; j < bwdtmp[i].size(); j++)
				{
					printf("fwdtmp[%d, 0, %d]: %.8f, a: %.8f, d: %.8f\t", i, j, fwdtmp[i][j], ann->Activation(fwdtmp[i][j]), ann->ActivationDerivative(fwdtmp[i][j]));
					printf("bwdtmp[%d, 0, %d]: %.8f\n", i, j, bwdtmp[i][j]);

				}
				printf("--------------------------------\n");
			}*/

			//changing weights
			for (int curLayer = 0; curLayer < ann->weights.size(); curLayer++)
			{
				for (int innum = 0; innum < ann->weights[curLayer].size(); innum++)
				{
					for (int onnum = 0; onnum < ann->weights[curLayer][innum].size(); onnum++)
					{
						ann->weights[curLayer][innum][onnum] += wd[curLayer][innum][onnum];
						//printf("wd[%d, %d, %d]: %.10f \t", curLayer, innum, onnum, wd[curLayer][innum][onnum]);
						wd[curLayer][innum][onnum] = 0;
					}
					//printf("\n");
				}
				//printf("-----\n");
			}

			/*for (int i = 0; i < outputs[inputNum].size(); i++)
			{
				std::cout << "learning: good " << outputs[inputNum][i] << " : try " << ann->Activation(fwdtmp.back()[i]) << std::endl;
				std::cout << "delta " << pow(outputs[inputNum][i] - ann->Activation(fwdtmp.back()[i]), 2) << std::endl;
			}*/
			
			//std::cout << "+++++++";
			//std::cout << std::endl;
			//printf("///////////////////////////////\n");
		}
		delta = sqrt(delta);

		if (learnStage % 1000 == 0)
		{
			printf("Delta: %f\n", delta);
		}
		
		//printf("_______________________________________________________________________________________\n");
		
	}
	ann->is_trained = true;
	return true;
}

void BPNN::setConfiguration(std::vector<int>& config, ActivationType activation_type)
{
	configuration = config;
	this->activation_type = activation_type;
	scale = 1;
	is_trained = false;
}

void BPNN::InitWeights()
{
	RandomInit();
	for (int i = 0; i < weights.size(); i++)
	{
		for (int j = 0; j < weights[i].size(); j++)
		{
			weights[i][j].push_back(static_cast<float>(rand()) / RAND_MAX - 0.5f);
		}
	}
}

void BPNN::ChangeWeight(int layer, int inum, int onum, float weight)
{
	weights[layer][inum][onum] = weight;
}

bool BPNN::Load(std::string filepath)
{
	std::ifstream file(filepath);
	if (!file.is_open()) return false;
	int buffer;
	const int CHAR_BUF_LEN = 100;
	char char_buffer[CHAR_BUF_LEN];
	file.getline(char_buffer, CHAR_BUF_LEN);
	std::string string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("activation type:"))
		throw "incorrect file format";
	file >> buffer;
	activation_type = (ActivationType)buffer;
	file.getline(char_buffer, CHAR_BUF_LEN);
	file.getline(char_buffer, CHAR_BUF_LEN);
	string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("activation scale:"))
		throw "incorrect file format";
	file >> scale;
	file.getline(char_buffer, CHAR_BUF_LEN);
	file.getline(char_buffer, CHAR_BUF_LEN);
	string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("configuration:"))
		throw "incorrect file format";
	file >> buffer;
	configuration.resize(buffer);
	for (size_t i = 0; i < configuration.size(); i++) {
		file >> configuration[i];
	}
	file.getline(char_buffer, CHAR_BUF_LEN);
	file.getline(char_buffer, CHAR_BUF_LEN);
	string_buffer = std::string(char_buffer);
	memset(char_buffer, 0, CHAR_BUF_LEN);
	if (string_buffer != std::string("weights:"))
		throw "incorrect file format";
	weights.resize(configuration.size() - 1);
	for (size_t i = 0; i < weights.size(); i++) 
	{
		weights[i].resize(configuration[i + 1]);
		for (size_t j = 0; j < weights[i].size(); j++) 
		{
			weights[i][j].resize(configuration[i] + 1);
			for (size_t k = 0; k < weights[i][j].size(); k++) 
			{
				file >> weights[i][j][k];
			}
		}
	}
	file.close();
	is_trained = true;
	return true;
}

bool BPNN::Save(std::string filepath)
{
	if (!is_trained) return false;
	std::ofstream file(filepath);
	if (!file.is_open()) return false;
	file << std::setprecision(9);
	file << "activation type:" << std::endl;
	file << (int)activation_type << std::endl;
	file << "activation scale:" << std::endl;
	file << scale << std::endl;
	file << "configuration:" << std::endl;
	file << configuration.size() << "\t";
	for each (int neuron_count in configuration) {
		file << neuron_count << "\t";
	}
	file << std::endl << "weights:" << std::endl;
	for (int curLayer = 0; curLayer < weights.size(); curLayer++)
	{
		for (int innum = 0; innum < weights[curLayer].size(); innum++)
		{
			for (int onnum = 0; onnum < weights[curLayer][innum].size(); onnum++)
			{
				file << weights[curLayer][innum][onnum] << " ";
			}
			file << std::endl;
		}
	}
	file.close();
	return true;
}

std::vector<std::vector<std::vector<float>>> BPNN::GetWeights()
{
	return weights;
}

std::shared_ptr<ANN::ANeuralNetwork> CreateNeuralNetwork(
	std::vector<int> & configuration = std::vector<int>(),
	ANeuralNetwork::ActivationType activation_type = ANeuralNetwork::POSITIVE_SYGMOID
)
{
	return std::shared_ptr<ANN::ANeuralNetwork>();
}


BPNN::BPNN()
{
	scale = 1;
	activation_type = ANeuralNetwork::POSITIVE_SYGMOID;
	is_trained = false;
}

BPNN::BPNN(BPNN *src)
{
	this->configuration = src->configuration;
	this->weights = src->weights;
	is_trained = false;
	scale = 1;
	this->activation_type = src->activation_type;
}

BPNN::BPNN(std::shared_ptr<BPNN> src)
{
	this->configuration = src->configuration;
	this->weights = src->weights;
	this->activation_type = src->activation_type;
	is_trained = false;
	scale = 1;
}

BPNN::~BPNN()
{
}

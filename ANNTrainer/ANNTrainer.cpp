#include <iostream>
#include <ANN.h>
#include <BPNN.h>
#include <ctime>

using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;
	srand(time(NULL));

	BPNN *nn = new BPNN();
	std::vector<std::vector<float>> trainInputs;
	std::vector<std::vector<float>> trainOutputs;
	if (!ANN::LoadData("xor.data", trainInputs, trainOutputs))
	{
		cout << "cant load file" << endl;
		system("pause");
		return -1;
	}
	std::cout << nn->GetType().c_str();

	nn->setConfiguration(std::vector<int>({ 2, 3, 3, 1}), ANeuralNetwork::BIPOLAR_SYGMOID);

	nn->Train(trainInputs, trainOutputs, 1000000, 0.001, 0.1, false);

	nn->Save("xor.bpnn");

	delete nn;

	system("pause");
	return 0;
}
#include <iostream>
#include <ANN.h>
#include <BPNN.h>
using namespace std;
using namespace ANN;

int main()
{
	cout << "hello ANN!" << endl;
	cout << GetTestString().c_str() << endl;

	BPNN *nn = new BPNN();

	if (!nn->Load("xor.bpnn"))
	{
		cout << "can't load file" << endl;
		system("pause");
		return 0;
	}

	std::cout << nn->GetType().c_str();	
	std::cout << "0\t0\t0\t" << nn->Predict(std::vector<float>({ 0, 0 }))[0] << std::endl;
	std::cout << "0\t1\t1\t" << nn->Predict(std::vector<float>({ 0, 1 }))[0] << std::endl;
	std::cout << "1\t0\t1\t" << nn->Predict(std::vector<float>({ 1, 0 }))[0] << std::endl;
	std::cout << "1\t1\t0\t" << nn->Predict(std::vector<float>({ 1, 1 }))[0] << std::endl;

	delete nn;

	system("pause");
	return 0;
}
﻿#include <iostream>
#include <FeatureExtraction.h>
#include <BlobProcessor.h>
#include <PolynomialManager.h>
#include <ZernikePolynomialManager.h>
#include "Visualisation.h"

using namespace std;
using namespace cv;

int main() 
{
	BlobProcessor bp;
	cout << "hello world!" << endl;
	cout << fe::GetTestString().c_str() << endl;

	int size = 50;
	int nMax = 12;

	cout << "\nEnter size: ";
	cin >> size;
	cout << "\nEnter n max: ";
	cin >> nMax;


	Mat src = imread("D://Downloads/numbers.png", 0);

	Mat dst = Mat::zeros(src.rows, src.cols, CV_8UC3);

	namedWindow("Source", 1);
	imshow("Source", src);

	vector<Mat> blobs;
	bp.DetectBlobs(src, blobs);
	bp.NormalizeBlobs(blobs, blobs, size);

	PolynomialManager *pm = new ZernikePolynomialManager();

	

	pm->InitBasis(nMax, size);
	ShowPolynomials("polynoms", pm->GetBasis());
	std::vector<ComplexMoments> moments(blobs.size());
	std::vector<cv::Mat> recoveredImages(blobs.size());
	for (int i = 0; i < blobs.size(); i++)
	{
		pm->Decompose(blobs[i], moments[i]);
		//imshow(std::to_string(i), blobs[i]);
		recoveredImages[i] = cv::Mat::zeros(blobs[i].size(), CV_64FC1);
		pm->Recovery(moments[i], recoveredImages[i]);
		ShowBlobDecomposition(std::to_string(i) + " rec", blobs[i], recoveredImages[i]);
	}

	waitKey(0);
	return 0;
}
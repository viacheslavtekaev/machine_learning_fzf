﻿#include "DigitsMomentsRecognizer.h"

using namespace cv;
using namespace cv::ml;

DigitsMomentsRecognizer::DigitsMomentsRecognizer()
{
	pAnn = cv::ml::ANN_MLP::create();
}


DigitsMomentsRecognizer::~DigitsMomentsRecognizer()
{
}

bool DigitsMomentsRecognizer::Train(std::map<std::string, std::vector<fe::ComplexMoments>> moments, std::vector<int> layers, int max_iters, float eps, float speed)
{
	Mat layersSize = Mat(layers.size() + 2, 1, CV_16U);
	unsigned int momentsCount = MomentsToInput(moments["0"].back()).cols;
	layersSize.row(0) = Scalar(momentsCount);
	for (int i = 0; i < layers.size(); i++)
	{
		layersSize.row(i + 1) = Scalar(layers[i]);
	}
	layersSize.row(layers.size() + 1) = Scalar(moments.size());
	
	// Конфигурация алгоритма обучения.
	pAnn->setLayerSizes(layersSize);
	pAnn->setBackpropMomentumScale(0.1);
	pAnn->setBackpropWeightScale(0.1);
	pAnn->setActivationFunction(cv::ml::ANN_MLP::SIGMOID_SYM, 0., 0.);
	// Критерий остановки обучения. 
	
	pAnn->setTrainMethod(cv::ml::ANN_MLP::RPROP, 0.001);

	cv::TermCriteria term_criteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, max_iters, eps);
	pAnn->setTermCriteria(term_criteria);
	

	unsigned int trainSamplesCount = 0;

	for (auto digitMoments : moments)
	{
		trainSamplesCount += digitMoments.second.size();
	}

	Mat train_data = Mat(trainSamplesCount, momentsCount, CV_32F);
	Mat train_labels = Mat(trainSamplesCount, moments.size(), CV_32F);
	
	unsigned int digit = 0;
	trainSamplesCount = 0;
	Mat inputMoments;
	for (auto digitMoments : moments)
	{
		for (int i = 0; i < digitMoments.second.size(); i++)
		{
			inputMoments = MomentsToInput(digitMoments.second[i]);
			for (int j = 0; j < momentsCount; j++)
			{
				train_data.at<float>(trainSamplesCount, j) = inputMoments.at<float>(0, j);
			}

			for (int j = 0; j < moments.size(); j++)
			{
				train_labels.at<float>(trainSamplesCount, j) = (j == digit ? 1.f : -1.f);
			}
			trainSamplesCount++;
		}
		digit++;
	}

	pAnn->train(train_data, ROW_SAMPLE, train_labels);
	return false;
}

cv::Mat DigitsMomentsRecognizer::MomentsToInput(fe::ComplexMoments &moments)
{
	Mat o;
	moments.abs.convertTo(o, CV_32F);
	return o;
}

std::string DigitsMomentsRecognizer::OutputToValue(cv::Mat output)
{
	int max = 0;
	for (int i = 0; i < output.cols; i++)
	{
		float oi = output.at<float>(0, i);
		float om = output.at<float>(0, max);
		if (oi > om)
		{
			max = i;
		}
	}

	return std::to_string(max);
}

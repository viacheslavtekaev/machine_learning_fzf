#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"

#include "FeatureExtraction.h"
#include "MomentsHelper.h"
#include <ZernikePolynomialManager.h>
#include <BlobProcessor.h>
#include "DigitsMomentsRecognizer.h"

using namespace cv;
using namespace std;
using namespace fe;

void generateData()
{
	cout << "===Generate data!===" << endl;
	MomentsHelper::DistributeData("../Data/labeled_data",
								  "../Data/ground_data",
								  "../Data/test_data", 
								  60);
	shared_ptr<IBlobProcessor> bp(new BlobProcessor());
	shared_ptr<PolynomialManager> zpm(new ZernikePolynomialManager());
	int nMax;
	int size;
	printf("\nEnter nMax: ");
	cin >> nMax;
	printf("\nEnter size: ");
	cin >> size;
	zpm->InitBasis(nMax, size);
	std::map< std::string, std::vector<fe::ComplexMoments> > m;
	MomentsHelper::GenerateMoments("../Data/ground_data", bp, zpm, m);
	MomentsHelper::SaveMoments("../Data/ground_moments.txt", m);

	m.clear();

	MomentsHelper::GenerateMoments("../Data/test_data", bp, zpm, m);
	MomentsHelper::SaveMoments("../Data/test_moments.txt", m);
}

void trainNetwork()
{
	cout << "===Train network!===" << endl;
	std::map< std::string, std::vector<fe::ComplexMoments> > m;
	MomentsHelper::ReadMoments("../Data/ground_moments.txt", m);
	DigitsMomentsRecognizer dmr;
	dmr.Train(m, { 50, 50}, 100000, 0.0001f, 0.1f);
	dmr.Save("../Data/nn.config");
}

void precisionTest()
{
	cout << "===Precision test!===" << endl;
	std::map< std::string, std::vector<fe::ComplexMoments> > m;
	MomentsHelper::ReadMoments("../Data/test_moments.txt", m);
	DigitsMomentsRecognizer dmr;
	dmr.Read("../Data/nn.config");
	printf("\nprecision result: %f", dmr.PrecisionTest(m));
}

void recognizeImage()
{
	cout << "===Recognize single image!===" << endl;
	DigitsMomentsRecognizer dmr;
	dmr.Read("../Data/nn.config");
	shared_ptr<IBlobProcessor> bp(new BlobProcessor());
	shared_ptr<PolynomialManager> zpm(new ZernikePolynomialManager());
	int nMax;
	int size;
	printf("\nEnter nMax: ");
	cin >> nMax;
	printf("\nEnter size: ");
	cin >> size;
	//zpm->InitBasis(nMax, size);
	//fe::ComplexMoments cm;
	//MomentsHelper::ProcessOneImage("../Data/numbers.png", bp, zpm, cm);
	//printf("\nResult : %s", dmr.Recognize(cm).c_str());

	std::string path;

	Mat src = imread("../Data/numbers.png", cv::IMREAD_GRAYSCALE);

	if (src.empty())
	{
		cout << "image not found";
		return;
	}

	namedWindow("Source", 1);
	imshow("Source", src);

	vector<Mat> blobs;
	bp->DetectBlobs(src, blobs);
	bp->NormalizeBlobs(blobs, blobs, size);

	zpm->InitBasis(nMax, size);
	std::vector<ComplexMoments> moments(blobs.size());
	for (int i = 0; i < blobs.size(); i++)
	{
		zpm->Decompose(blobs[i], moments[i]);
		putText(blobs[i], dmr.Recognize(moments[i]), Point(blobs[i].rows/2, blobs[i].cols/2), FONT_HERSHEY_DUPLEX, 1, Scalar(143, 143, 0), 1);
		imshow(std::to_string(i), blobs[i]);
	}
	waitKey(0);
	
}

int main(int argc, char** argv)
{
	string key;
	do 
	{
		cout << "===Enter next values to do something:===" << endl;
		cout << "  '1' - to generate data." << endl;
		cout << "  '2' - to train network." << endl;
		cout << "  '3' - to check recognizing precision." << endl;
		cout << "  '4' - to recognize single image." << endl;
		cout << "  'exit' - to close the application." << endl;
		cin >> key;
		cout << endl;
		if (key == "1") {
			generateData();
		}
		else if (key == "2") {
			trainNetwork();
		}
		else if (key == "3") {
			precisionTest();
		}
		else if (key == "4") {
			recognizeImage();
		}
		cout << endl;
	} while (key != "exit");
	return 0;
}
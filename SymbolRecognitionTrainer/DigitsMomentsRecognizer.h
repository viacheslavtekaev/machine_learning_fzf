﻿#pragma once
#include "MomentsRecognizer.h"
#include <opencv2/core/types.hpp>

class DigitsMomentsRecognizer :
	public MomentsRecognizer
{
public:
	DigitsMomentsRecognizer();
	~DigitsMomentsRecognizer();

	/**Обучить нейронную сеть.
 * @param moments - набор обучающих данных. Это ассоциативный массив.
 *					Ключ - значение символа (например "5")
 *                  Значение - набор разложений разчных вариаций этого символа.
 * @param layers - конфигурация скрытых слоев будущего персептрона.
 *					Ключ - номер СКРЫТОГО слоя.
 *					Значение - количество нейронов в слое.
 * @param max_iters - максимальное количество итераций при обучении.
 * @param eps - требуемая точность распознавания на обучающей выборке.
 * @param speed - скорость обучения = коэффициент перед корректировкой к весу.
 * @return true - сеть успешно обучена, false - сеть не обучена.
 */
	bool Train(
		std::map<std::string, std::vector<fe::ComplexMoments>> moments,
		std::vector<int> layers,
		int max_iters = 100000,
		float eps = 0.1,
		float speed = 0.1) override;

	// Inherited via MomentsRecognizer
	virtual cv::Mat MomentsToInput(fe::ComplexMoments & moments) override;
	virtual std::string OutputToValue(cv::Mat output) override;
};


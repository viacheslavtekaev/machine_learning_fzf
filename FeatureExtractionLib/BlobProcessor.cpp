#define FEATURE_DLL_EXPORTS
#include "BlobProcessor.h"


using namespace cv;
using namespace std;

BlobProcessor::BlobProcessor()
{

}

BlobProcessor::~BlobProcessor()
{

}

void BlobProcessor::DetectBlobs(cv::Mat image, std::vector<cv::Mat> &blobs)
{
	image = ~image;
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;
	findContours(image, contours, hierarchy, RETR_CCOMP, CHAIN_APPROX_SIMPLE);
	// iterate through all the top-level contours,
	// draw each connected component with its own random color
	
	for (int idx = 0; idx >= 0; idx = hierarchy[idx][0])
	{
		Scalar color(255, 255, 255);
		float radius = 0;
		Point2f center;
		cv::minEnclosingCircle(contours[idx], center, radius);
		blobs.push_back(Mat::zeros(radius * 2, radius * 2, CV_8UC1));
		drawContours(blobs.back(), contours, idx, color, FILLED, 8, hierarchy, 2147483647, -center + Point2f(radius, radius));
	}
}

void BlobProcessor::NormalizeBlobs(
	std::vector<cv::Mat> & blobs,
	std::vector<cv::Mat> & normalized_blobs,
	int side)
{
	normalized_blobs.resize(blobs.size());
	for (int i = 0; i < blobs.size(); i++)
	{
		cv::resize(blobs[i], normalized_blobs[i], cv::Size(side, side));
	}
}

std::string BlobProcessor::GetType()
{
	return "BLOB PROCESSOR";
}

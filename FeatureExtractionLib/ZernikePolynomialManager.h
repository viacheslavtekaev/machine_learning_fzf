#pragma once
#include "PolynomialManager.h"
#include "ComplexMoments.h"
#include "RadialFunctions.h"

using namespace fe;

class ZernikePolynomialManager :
	public fe::PolynomialManager
{
public:
	FEATURE_DLL_API ZernikePolynomialManager();
	FEATURE_DLL_API ~ZernikePolynomialManager();


	// Inherited via PolynomialManager
	FEATURE_DLL_API virtual void Decompose(cv::Mat blob, ComplexMoments & decomposition) override;
	FEATURE_DLL_API virtual void Recovery(ComplexMoments & decomposition, cv::Mat & recovery) override;
	FEATURE_DLL_API virtual void InitBasis(int n_max, int diameter) override;
	FEATURE_DLL_API virtual std::string GetType() override;
};


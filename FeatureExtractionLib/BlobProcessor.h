#pragma once
#include "IBlobProcessor.h"
#include "ExportMacro.h"

class BlobProcessor :
	public fe::IBlobProcessor
{
public:
	FEATURE_DLL_API BlobProcessor();
	FEATURE_DLL_API ~BlobProcessor();
	FEATURE_DLL_API std::string GetType();
	FEATURE_DLL_API void NormalizeBlobs(
		std::vector<cv::Mat> & blobs,
		std::vector<cv::Mat> & normalized_blobs,
		int side);
	FEATURE_DLL_API void DetectBlobs(cv::Mat image, std::vector<cv::Mat> & blobs);
};


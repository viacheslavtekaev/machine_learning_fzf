#define FEATURE_DLL_EXPORTS
#include "ZernikePolynomialManager.h"




ZernikePolynomialManager::ZernikePolynomialManager()
{
}


ZernikePolynomialManager::~ZernikePolynomialManager()
{
}

void ZernikePolynomialManager::InitBasis(int n_max, int diameter)
{
	double rad = 0;
	double angle = 0;
	double zerVal = 0;
	polynomials.resize(n_max);
	//polynomials[0].resize(1);
	for (int n = 0; n < n_max; n++)
	{
		/*if (n != 0)
		{
			polynomials[n].resize(((n + 1) % 2 != 0) ? polynomials[n - 1].size() : (polynomials[n - 1].size() + 1));
		}*/

		polynomials[n].resize(n + 1);	
		for (int m = 0; m < polynomials[n].size(); m++)
		{	
			polynomials[n][m].first = cv::Mat::zeros(diameter, diameter, CV_64FC1);
			polynomials[n][m].second = cv::Mat::zeros(diameter, diameter, CV_64FC1);
			for (int y = 0; y < diameter; y++)
			{
				for (int x = 0; x < diameter; x++)
				{
					rad = 2. * sqrt(pow(x - diameter / 2, 2) + pow(y - diameter / 2, 2)) / diameter;
					angle = atan2(static_cast<double>(y - diameter / 2), static_cast<double>(x - diameter / 2));
					zerVal = rf::RadialFunctions::Zernike(rad, n, m);
					polynomials[n][m].first.at<double>(x, y) = zerVal * cos(m * angle);
					polynomials[n][m].second.at<double>(x, y) = zerVal * sin(m * angle);
				}
			}
		}
	}

}

std::string ZernikePolynomialManager::GetType()
{
	return std::string();
}

void ZernikePolynomialManager::Recovery(ComplexMoments & decomposition, cv::Mat & recovery)
{
	for (int n = 0, i = 0; n < polynomials.size(); n++)
	{
		for (int m = 0; m < polynomials[n].size(); m++)
		{
			recovery += polynomials[n][m].first * decomposition.re.at<double>(0, i) + polynomials[n][m].second * decomposition.im.at<double>(0, i);
			i++;
		}
	}
}

void ZernikePolynomialManager::Decompose(cv::Mat blob, ComplexMoments & decomposition)
{
	int size = 0;
	for (int n = 0; n < polynomials.size(); n++)
	{
		size += polynomials[n].size();
	}

	cv::Mat cBlob = cv::Mat(blob.size(), CV_64FC1);
	blob.convertTo(cBlob, CV_64FC1, 1.0 / 255.0 / blob.cols / blob.cols);

	decomposition.re = cv::Mat::zeros(1, size, CV_64FC1);
	decomposition.im = cv::Mat::zeros(1, size, CV_64FC1);
	decomposition.phase = cv::Mat::zeros(1, size, CV_64FC1);
	decomposition.abs = cv::Mat::zeros(1, size, CV_64FC1);

	for (int n = 0, i = 0; n < polynomials.size(); n++)
	{
		for (int m = 0; m < polynomials[n].size(); m++)
		{
			if (!polynomials[n][m].first.empty())
			{
				decomposition.re.at<double>(0, i) = cv::sum(cBlob.mul(polynomials[n][m].first))[0];
			}
			else
			{
				decomposition.re.at<double>(0, i) = 0;
			}
			if (!polynomials[n][m].second.empty())
			{
				decomposition.im.at<double>(0, i) = cv::sum(cBlob.mul(polynomials[n][m].second))[0];
			}
			else
			{
				decomposition.im.at<double>(0, i) = 0;
			}
			decomposition.abs.at<double>(0, i) = sqrt(pow(decomposition.re.at<double>(0, i), 2) + pow(decomposition.im.at<double>(0, i), 2));
			//decomposition.phase = atan2(decomposition.im.at<double>(0, i), decomposition.re.at<double>(0, i));
			i++;
		}
	}

	double norm = cv::norm(decomposition.abs, cv::NORM_L1);
	decomposition.re /= norm;
	decomposition.im /= norm;
}

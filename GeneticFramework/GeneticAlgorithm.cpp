#include <algorithm>
#include "GeneticAlgorithm.h"

using namespace ga;
using namespace std;

ga::GeneticAlgorithm::GeneticAlgorithm()
{
}


ga::GeneticAlgorithm::~GeneticAlgorithm()
{
}

ga::pEpoch ga::GeneticAlgorithm::Selection(double alive_perc, double mutation_perc, double crossover_perc)
{
	// Сортировка особей по набранным очкам.
	sort(epoch->population.begin(), epoch->population.end(),
		[](std::pair<int, pIIndividual> a, std::pair<int, pIIndividual> b)
	{
		return a.first > b.first;
	});

	int aliveCount = (int)round(alive_perc/100. * epoch->population.size());
	int mutationCount = (int)round(mutation_perc/100. * epoch->population.size());
	int crossoveredCount = (int)round(crossover_perc/100. * epoch->population.size());
	int unchangedCount = epoch->population.size() - (mutationCount + crossoveredCount);

	auto new_epoch = std::make_shared < Epoch >();
	new_epoch->population.resize(epoch->population.size());

	for (unsigned int i = 0; i < epoch->population.size();)
	{
		switch (rand() % 3)
		{
		//unchanged
		case 0:
			if (unchangedCount <= 0)
			{
				continue;
			}
			new_epoch->population[i].second = epoch->population[rand() % aliveCount].second->Clone();
			unchangedCount--;
			i++;
			break;
		//mutation
		case 1:
			if (mutationCount <= 0)
			{
				continue;
			}
			new_epoch->population[i].second = epoch->population[rand() % epoch->population.size()].second->Mutation();
			mutationCount--;
			i++;
			break;
		//crossovered
		case 2:
			if (crossoveredCount <= 0)
			{
				continue;
			}
			new_epoch->population[i].second = 
				epoch->population[rand() % epoch->population.size()].second->Crossover(
			    epoch->population[rand() % epoch->population.size()].second);
			crossoveredCount--;
			i++;
			break;
		}
	}

	return std::move(new_epoch);
}


#include "Individual.h"
#include "XNOGame.h"
#include <iostream>

ga::Individual::Individual()
{
	nn = std::make_shared<BPNN>();
	nn->setConfiguration(std::vector<int>({ 9, 15, 15, 1 }), ANeuralNetwork::POSITIVE_SYGMOID);
	nn->InitWeights();
}

ga::Individual::Individual(ga::Individual &clone)
{
	nn = std::make_shared<BPNN>(clone.nn);
	nn->InitWeights();
}


ga::Individual::~Individual()
{
}

std::shared_ptr<ga::IIndividual> ga::Individual::Mutation()
{
	std::shared_ptr<ga::Individual> clone = std::static_pointer_cast<ga::Individual>(this->Clone());
	std::vector<std::vector<std::vector<float>>> wCopy = clone->nn->GetWeights();
	float maxDeviation = 20.f;
	float deviationPercent = 0.f;
	for (unsigned int curLayer = 0; curLayer < wCopy.size(); curLayer++)
	{
		for (unsigned int innum = 0; innum < wCopy[curLayer].size(); innum++)
		{
			for (unsigned int onnum = 0; onnum < wCopy[curLayer][innum].size() - 1; onnum++)
			{
				deviationPercent = static_cast<float>(rand()) / RAND_MAX;
				float newWeight = wCopy[curLayer][innum][onnum] * (1 + maxDeviation * deviationPercent);
				clone->nn->ChangeWeight(curLayer, innum, onnum, newWeight);
			}
		}
	}
	
	return clone;
}

std::shared_ptr<ga::IIndividual> ga::Individual::Crossover(std::shared_ptr<IIndividual> individual)
{
	std::shared_ptr<ga::Individual> clone = std::static_pointer_cast<ga::Individual>(this->Clone());
	std::shared_ptr<ga::Individual> alien = std::static_pointer_cast<ga::Individual>(individual);
	std::vector<std::vector<std::vector<float>>> wCopy = alien->nn->GetWeights();
	unsigned int crosslayer = rand() % wCopy.size();

	for (unsigned int innum = 0; innum < wCopy[crosslayer].size(); innum++)
	{
		for (unsigned int onnum = 0; onnum < wCopy[crosslayer][innum].size() - 1; onnum++)
		{
			clone->nn->ChangeWeight(crosslayer, innum, onnum, wCopy[crosslayer][innum][onnum]);
		}
	}
	return clone;
}

std::pair<int, int> ga::Individual::Spare(pIIndividual individual)
{

	std::pair<int, int> result(0, 0);

	if (this == individual.get())
	{
		return result;
	}

	XNOGame game;
	
	Cell winner;
	Cell currentCell;
	float bestTurnScore = 0;
	int bestTurnPos = 0;
	Field f;
	int fieldSize = game.GetFieldSize();
	fieldSize *= fieldSize;
	float decision = 0;
	for (int i = 0; i < SPARES_COUNT; i++)
	{
		//std::cout << "\nSTART SPARE\n";
		game.Reset();
		winner = cellE;
		//First step {cellX : this, cellO : alien)
		currentCell = game.GetFirstCell();
		do
		{
			bestTurnScore = 0;
			bestTurnPos = 0;
			//Try all possible turns
			for (unsigned int itry = 0; itry < fieldSize; itry++)
			{
				//Clone original field
				f = game.GetField();
				//Make virtual turn
				if(!game.MakeTurn(&f, currentCell, itry))
				{
					continue;
				}
				//Evaluate turn
				if (currentCell == cellX)
				{
					decision = this->MakeDecision(std::vector<float>(f.begin(), f.end()));
				}
				else
				{
					for (unsigned int j = 0; j < f.size(); j++)
					{
						f[j] = -f[j];
					}
					decision = individual->MakeDecision(std::vector<float>(f.begin(), f.end()));
				}
				//Looking for the best turn
				if (decision > bestTurnScore)
				{
					bestTurnScore = decision;
					bestTurnPos = itry;
				}
			}
			//When the best turn was found make it.
			game.MakeTurn(currentCell, bestTurnPos);

			//game.DrawField();
			//Change player
			currentCell = -currentCell;
		} while ((winner = game.GetWinner()) == cellE && !game.IsDraw());
		//Counting wins amount
		if(winner == cellX)
		{//this individ
			result.first += 5;
			result.second -= 2;
			//std::cout << "\nX WIN\n";
		}
		else if(winner == cellO)
		{//alien individ
			result.second += 5;
			result.first -= 2;
			//std::cout << "\nO WIN\n";
		}
		else
		{
			//std::cout << "\nDRAW\n";
		}
	}
	return result;
}

float ga::Individual::MakeDecision(std::vector<float>& input)
{
	return nn->Predict(input)[0];
}

std::shared_ptr<ga::IIndividual> ga::Individual::Clone()
{
	return std::make_shared<ga::Individual>(*this);
}

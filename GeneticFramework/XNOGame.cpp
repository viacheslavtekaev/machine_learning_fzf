#include "XNOGame.h"
#include <cstdio>
#include <cmath>
#include <random>
#include <ctime>


XNOGame::XNOGame()
{
	this->fieldSize = 3;
	srand(time(NULL));
	field = new std::vector<Cell>(fieldSize * fieldSize);
	Init();
}

void XNOGame::Reset()
{
	Init();
}

int XNOGame::GetFieldSize()
{
	return fieldSize;
}

void XNOGame::Init()
{
	for (unsigned int i = 0; i < field->size(); i++)
	{
		(*field)[i] = cellE;
	}
	isDraw = false;
	turnsCount = 0;
}

XNOGame::~XNOGame()
{
	delete field;
}


bool XNOGame::MakeTurn(Field *f, Cell c, unsigned int pos)
{
	if (CheckTurn(f, c, pos))
	{
		f->at(pos) = c;
		return true;
	}
	return false;
}

bool XNOGame::MakeTurn(Cell c, unsigned int pos)
{
	if (MakeTurn(field, c, pos))
	{
		turnsCount++;
		return true;
	}
	return false;
}

bool XNOGame::MakeTurn(Cell c, unsigned int posX, unsigned int posY)
{
	if (MakeTurn(c, posY * fieldSize + posX))
	{
		turnsCount++;
		return true;
	}
	return false;
}

bool XNOGame::CheckTurn(Field * f, Cell c, unsigned int pos)
{
	return (pos < f->size() && f->at(pos) == cellE);
}

Cell XNOGame::GetWinner()
{
	if (field->at(0) == field->at(1) && field->at(1) == field->at(2))
	{
		return field->at(0);
	}
	if (field->at(3) == field->at(4) && field->at(4) == field->at(5))
	{
		return field->at(3);
	}
	if (field->at(6) == field->at(7) && field->at(7) == field->at(8))
	{
		return field->at(6);
	}
	if (field->at(0) == field->at(3) && field->at(3) == field->at(6))
	{
		return field->at(0);
	}
	if (field->at(1) == field->at(4) && field->at(4) == field->at(7))
	{
		return field->at(1);
	}
	if (field->at(2) == field->at(5) && field->at(5) == field->at(8))
	{
		return field->at(2);
	}
	if (field->at(0) == field->at(4) && field->at(4) == field->at(8))
	{
		return field->at(0);
	}
	if (field->at(2) == field->at(4) && field->at(4) == field->at(6))
	{
		return field->at(2);
	}

	if (turnsCount >= field->size())
	{
		isDraw = true;
	}

	return cellE;
}

bool XNOGame::IsDraw()
{
	return isDraw;
}

void XNOGame::DrawField()
{
	for (int y = 0; y < fieldSize; y++)
	{
		printf("\n|");
		for (int x = 0; x < fieldSize; x++)
		{
			printf("%c|", GetCellDesc(field->at(y * fieldSize + x)));
		}
	}
}

Field XNOGame::GetField()
{
	return (*field);
}

Cell XNOGame::GetFirstCell()
{
	return ((rand() > RAND_MAX/2) ? cellO : cellX);
}

char XNOGame::GetCellDesc(Cell c)
{
	switch (c)
	{
	case cellX:
		return 'X';
	case cellO:
		return 'O';
	}
	return ' ';
}



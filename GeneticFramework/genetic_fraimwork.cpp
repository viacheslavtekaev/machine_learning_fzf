#include <iostream>
#include <algorithm>
#include "GeneticAlgorithm.h"
#include "XNOGame.h"
#include "Player.h"
using  namespace std;
using namespace ga;

int main()
{
	cout << "Hello!" << endl;
	XNOGame game;

	/*Cell winner = cellE;
	int turnPos = 0;
	Cell firstCell = game.GetFirstCell();
	do
	{
		//game.DrawField();
		do
		{
			//std::cout << "\nEnter " << game.GetCellDesc(firstCell) << " pos: ";
			//std::cin >> turnPos;
		} while (!game.MakeTurn(firstCell, turnPos));
		firstCell = -firstCell;
	} while ((winner = game.GetWinner()) == cellE);*/


	//std::cout << "\nWinner: " << game.GetCellDesc(winner) << std::endl;
	int epochCount = 10;
	int populationSize = 10;
	int alivePerc = 60;
	int mutationPerc = 10;
	int crossPerc = 60;
	GeneticAlgorithm genAlg;
	genAlg.epoch = std::make_shared<Epoch>(populationSize);

	for (int i = 0; i < epochCount; i++)
	{
		genAlg.epoch->EpochBattle();
		if (i != 0 && i % 10 == 0)
		{
			std::cout << "\n100 rounds done";
		}
		if (i == epochCount - 1)
		{
			sort(genAlg.epoch->population.begin(), genAlg.epoch->population.end(),
				[](std::pair<int, pIIndividual> a, std::pair<int, pIIndividual> b)
			{
				return a.first > b.first;
			});
			break;
		}
		genAlg.epoch = genAlg.Selection(alivePerc, mutationPerc, crossPerc);
	}

	Player player;
	do {
		player.Spare(genAlg.epoch->population[0].second);
	} while (true);
	
	system("pause");
	return 0;
}
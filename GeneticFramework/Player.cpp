#include "Player.h"
#include "XNOGame.h"
#include <iostream>

using namespace ga;

Player::Player()
{
}


Player::~Player()
{
}

std::shared_ptr<IIndividual> ga::Player::Mutation()
{
	return std::shared_ptr<IIndividual>(nullptr);
}

std::shared_ptr<IIndividual> ga::Player::Crossover(std::shared_ptr<IIndividual> individual)
{
	return std::shared_ptr<IIndividual>(nullptr);
}

std::pair<int, int> ga::Player::Spare(std::shared_ptr<IIndividual> individual)
{
	XNOGame game;
	std::pair<int, int> result(0, 0);
	Cell winner;
	Cell currentCell;
	float bestTurnScore = 0;
	int bestTurnPos = 0;
	Field f;
	float decision = 0;
	int fieldSize = game.GetFieldSize();
	fieldSize *= fieldSize;

	winner = cellE;
	//First step {cellX : this, cellO : alien)
	currentCell = game.GetFirstCell();
	do
	{
		bestTurnScore = 0;
		bestTurnPos = 0;

		if (currentCell == cellX)
		{
			std::cout << "\nEnter your turn pos: ";
			std::cin >> bestTurnPos;
		}
		else
		{
			//Try all possible turns
			for (unsigned int itry = 0; itry < fieldSize; itry++)
			{
				//Clone original field
				f = game.GetField();
				//Make virtual turn
				if (!game.MakeTurn(&f, currentCell, itry))
				{
					continue;
				}

				for (unsigned int i = 0; i < f.size(); i++)
				{
					f[i] = -f[i];
				}
				//Estimate turn
				decision = individual->MakeDecision(std::vector<float>(f.begin(), f.end()));

				//Looking for the best turn
				if (decision > bestTurnScore)
				{
					bestTurnScore = decision;
					bestTurnPos = itry;
				}
			}
		}
		//When the best turn was found make it.
		game.MakeTurn(currentCell, bestTurnPos);

		//Draw field
		game.DrawField();
		std::cout << std::endl;
		//Change player
		currentCell = -currentCell;
	} while ((winner = game.GetWinner()) == cellE && !game.IsDraw());

	//Counting wins amount
	if (winner == cellX)
	{
		result.first++;//this individ
		std::cout << "\nYOU WIN!\n";
	}
	else if (winner == cellO)
	{
		result.second++;//alien individ
		std::cout << "\nYOU LOOSE!\n";
	}
	std::cout << "\nDRAW!\n";
	return result;
}

float ga::Player::MakeDecision(std::vector<float>& input)
{
	return 0;
}

std::shared_ptr<IIndividual> ga::Player::Clone()
{
	return std::shared_ptr<IIndividual>(nullptr);
}


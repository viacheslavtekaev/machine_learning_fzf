#pragma once
#include "IIndividual.h"

namespace ga
{
	class Player :
		public IIndividual
	{
	public:
		Player();
		~Player();

		// Inherited via IIndividual
		virtual std::shared_ptr<IIndividual> Mutation() override;
		virtual std::shared_ptr<IIndividual> Crossover(std::shared_ptr<IIndividual> individual) override;
		virtual std::pair<int, int> Spare(std::shared_ptr<IIndividual> individual) override;
		virtual float MakeDecision(std::vector<float>& input) override;
		virtual std::shared_ptr<IIndividual> Clone() override;
	};
}


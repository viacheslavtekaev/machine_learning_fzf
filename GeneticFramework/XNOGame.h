#pragma once
#include <vector>

typedef char Cell;
typedef std::vector<Cell> Field;

static const Cell cellX = -1;
static const Cell cellO =  1;
static const Cell cellE =  0;

class XNOGame
{
public:
	XNOGame();
	~XNOGame();

	bool MakeTurn(Field *f, Cell c, unsigned int pos);
	bool MakeTurn(Cell c, unsigned int pos);	
	bool MakeTurn(Cell c, unsigned int posX, unsigned int posY);
	bool CheckTurn(Field *f, Cell c, unsigned int pos);
	Cell GetWinner();
	bool IsDraw();
	void DrawField();
	Field GetField();
	Cell GetFirstCell();
	char GetCellDesc(Cell c);
	void Reset();
	int GetFieldSize();

private:
	bool isDraw;
	void Init();
	int fieldSize;
	int turnsCount;
	Field *field;
};

